<?php

namespace backend\controllers\guardian;

use backend\controllers\BaseController;
use common\components\BitHelper;
use common\components\bl\GuardianDashboardManager;
use common\components\DateHelper;
use common\components\Formatter;
use common\helpers\ArrayHelper;
use common\models\DocEvent;
use common\models\guardian\SelectStudentForm;
use common\models\guardian\UpcomingEvent;
use common\models\Holiday;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\PasswordChangeForm;

class Yii2GuardianController extends BaseController
{


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $manager = $this->getDashboardManager();

        $pendingDataProvider = new ArrayDataProvider([
            'allModels' => $manager->getPendingEvents(),
            'pagination' => false,
        ]);

        $actor = \Yii::$container->get('Actor');
        $students = ArrayHelper::map($actor->students, 'id', 'fullName');
        $students[0] = 'all children';
        ksort($students);

        $selectStudentForm = new SelectStudentForm();
        $selectStudentForm->studentId = \Yii::$app->session->get('selectedStudent', 0);
        $selectStudentForm->returnUrl = \Yii::$app->request->url;

        return $this->render('index', [
            'upcomingDataProvider' => $this->getUpcomingDataProvider(),
            'pendingDataProvider' => $pendingDataProvider,
            'events' => Json::encode(iterator_to_array($this->getCalendarItems())),
            'productHistoryDataProvider' => $this->getProductHistoryDataProvider(),
            'formHistoryDataProvider' => $this->getFormHistoryDataProvider(),
            'selectStudentForm' => $selectStudentForm,
            'students' => $students,
        ]);
    }


    /**
     * @return \Generator
     * @throws \yii\web\ForbiddenHttpException
     */
    private function getCalendarItems()
    {
        $colors = [
            UpcomingEvent::STATUS_DECLINED => '#000',
            UpcomingEvent::STATUS_INCOMPLETE => '#1D84C6',
            UpcomingEvent::STATUS_PLANNED => '#1BB394',
            UpcomingEvent::STATUS_PENDING_OPTIONAL => '#F8AC5A',
            UpcomingEvent::STATUS_PENDING_REQUIRED => '#ED5666',
            UpcomingEvent::STATUS_PENDING_CANCELED => '#9c9c9c',
        ];


        $daysOfWeek = DocEvent::getDaysOfWeek();
        $holidayDates = Holiday::find()
            ->select('start_date, end_date')
            ->asArray()
            ->all();
        $holidays = [];
        foreach ($holidayDates as $dates) {
            $holidays = ArrayHelper::merge($holidays, DateHelper::getDaysBetweenDatesIncluded($dates['start_date'], $dates['end_date']));
        }
        $holidays = array_flip($holidays);


        foreach ($this->getDashboardManager()->getUpcomingEvents() as $event) {
            if (!empty($event->predecessor)) {
                $eventStartDate = Formatter::date($event->start_date);
                $eventEndDate = Formatter::date($event->end_date);
                $current = DateHelper::beginningOfWeek($eventStartDate);
                $startDate = null;
                $previousDate = null;
                foreach ($daysOfWeek as $dayOfWeek => $name) {
                    $date = $current
                        ->modify($name)
                        ->format(Formatter::DATE_FORMAT);
                    if (DateHelper::between($date, $eventStartDate, $eventEndDate) &&
                        !key_exists($date, $holidays) &&
                        (($event->repeat_on_days & $dayOfWeek) > 0)) {
                        if (empty($startDate)) {
                            $startDate = $date;
                        }
                        $previousDate = $date;
                        continue;
                    }

                    if (empty($startDate)) {
                        continue;
                    }

                    yield [
                        'title' => $event->title,
                        'start' => $this->newDateExpression($startDate . ' 00:00:00'),
                        'end' => $this->newDateExpression($previousDate . ' 23:23:59'),
                        'color' => $colors[$event->getUpcomingStatus()],
                    ];
                    $startDate = null;
                }
                if (!empty($startDate)) {
                    yield [
                        'title' => $event->title,
                        'start' => $this->newDateExpression($startDate . ' 00:00:00'),
                        'end' => $this->newDateExpression($previousDate . ' 23:23:59'),
                        'color' => $colors[$event->getUpcomingStatus()],
                    ];
                }

                continue;
            }

            $params = [
                'title' => $event->title,
                'start' => $this->newDateExpression($event->start_date),
                'end' => $this->newDateExpression($event->end_date),
                'color' => $colors[$event->getUpcomingStatus()],
            ];
            if (
                empty($event->predecessor)
                && (BitHelper::testBits($event->type,
                    DocEvent::EVENT_TYPE_FIELD_TRIP | DocEvent::EVENT_TYPE_STOCK_THE_CLASSROOM)
                )) {
                $params['url'] = $event->status === DocEvent::STATUS_CANCELED ? '#' : Url::toRoute($event->getRoute());
            }
            yield $params;
        }
    }
}