<?php

namespace App\Services\PayKeeper\Models;


class User
{
    /**
     * Имя пользователя
     * @var string
     */
    public $login;

    /**
     * Имя пользователя
     * @var string
     */
    public $user;

    /**
     * Количество возвратов, которое пользователь может осуществить за сутки.
     * @var string
     */
    public $refund;

    /**
     * Админитратор
     * @var boolean
     */
    public $admin;

    /**
     * Возможность осуществлять возвраты. Принимает значения: true/false.
     * @var boolean
     */
    public $refund_allow;

    /**
     * Setting constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        // set values
        foreach ($parameters as $key => $parameter) {
            if (property_exists($this, $key)) {
                if ($key == 'user') {
                    $this->login = $parameter;
                }
                if ($key == 'login') {
                    $this->user = $parameter;
                }
                $this->{$key} = $parameter;
            }
        }
    }
}