<?php


namespace App\Services\PayKeeper\Models;


class Payment
{
    /**
     * Уникальный номер созданного счёта
     * @var integer
     * @example '3154862'
     */
    public $id;
    /**
     * Статус счета
     * @var float
     * @example "1.00"
     */
    public $pay_amount;
    /**
     * Сумма возврата
     * @var float
     * @example '0.00'
     */
    public $refund_amount;
    /**
     * Клиент
     * @var string
     * @example "Стамакс Юрий"
     */
    public $clientid;
    /**
     * Идентификатор заказа
     * @var string
     * @example "145"
     */
    public $orderid;
    /**
     * Идентификатор платежной системы
     * @var integer
     * @example '245875'
     */
    public $payment_system_id;
    /**
     * Системное название платежной системы
     * @var string
     * @example 'Сбербанк',
     */
    public $site_description;
    /**
     * Название платежной системы
     * @var string
     * @example 'SberBank'
     */
    public $system_description;
    /**
     * Уникальный идентификатор транзакции
     * @var string|null
     * @example '15595739421398' OR NULL
     */
    public $unique_id;
    /**
     * Уникальный идентификатор транзакции (Отменён/Cовершён/Не состоялся/Ожидает оплаты)
     * @var string
     * @example 'canceled' OR 'success' OR 'failed' OR 'pending'
     */
    public $status;
    /**
     * Количество отправленных запросов при проведении платежа в систему магазина
     * @var integer
     * @example '0'
     */
    public $repeat_counter;
    /**
     * Дата/Время создания платежа
     * @var string
     * @example '2019-05-31 14:57:43'
     */
    public $pending_datetime;
    /**
     * Дата/Время проведения платежа
     * @var string|null
     * @example '2019-06-04 12:08:30' or NULL
     */
    public $obtain_datetime;
    /**
     * Дата/Время информирования магазина о проведенном платеже
     * @var string|null
     * @example '2019-06-04 12:08:30' or NULL
     */
    public $success_datetime;
    /**
     * Текст ошибки
     * @var string
     */
    protected $msg;
    /**
     * Результат выволнения
     * @var string
     * @example 'success' OR 'fail'
     */
    protected $result;

    /**
     * Bill constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        // set values
        foreach ($parameters as $key => $parameter) {
            if (property_exists($this, $key)) {
                if ($key === 'id') {
                    $this->result = 'success';
                }

                $this->{$key} = $parameter;
            }
        }
    }

    /**
     * Если результат "Успех"
     * @return bool
     */
    public function isSuccess()
    {
        return $this->result == 'success';
    }

    /**
     * Если результат "Збой"
     * @return bool
     */
    public function isFail()
    {
        return $this->result == 'fail';
    }

    /**
     * Получаем ответ с текстом ошибки
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }
}