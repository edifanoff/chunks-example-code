<?php

namespace App\Services\PayKeeper\Models;


class Setting
{
    /**
     * Название торговой точки
     * Public
     * @var string
     */
    public $shopname = null;
    /**
     * URL-сайта торговой точки
     * Public
     * @var string
     */
    public $shopdomain = null;
    /**
     * Юридическое наименование организации
     * Public
     * @var string
     */
    public $legal_name = null;
    /**
     * Номер договора
     * Public
     * @var string
     */
    public $paykeeper_id = null;
    /**
     * Почта для обращения в организацию
     * Protected
     * @var string
     */
    public $support_email; //": "support@paykeeper.ru",
    /**
     * Режим работы информера, принимает значение post или email
     * Protected
     * @var string
     */
    public $informer_type;
    /**
     * URL уведомления для информера по которому отправляется информация о принятом платеже
     * Protected
     * @var string
     */
    public $informer_url;
    /**
     * Секретное слово для подписи сообщений информера
     * Protected
     * @var string
     */
    public $informer_seed;
    /**
     * Флаг использования стандартных настроек SMTP сервера PayKeeper. Если указано значение ‘true’,
     * то для отправки писем используются настройки указанные в параметрах smtp_host, smtp_port, smtp_user, smtp_password.
     * Если указано ‘false’, то используются настройки почтового сервера PayKeeper по умолчанию и параметры
     * smtp_host, smtp_port, smtp_user, smtp_password не возвращаются в запросе.
     *
     * Protected
     * @var string
     */
    public $smtp_use_custom;
    /**
     * Флаг указывает на то, что нужно пропускать стандартные страницы возврата paykeeper и сразу отправлять пользователя на сайт торговой точки. Принимает значения on или off
     * Protected
     * @var string
     */
    public $skip_default_pages;
    /**
     * URL для возврата в случае успешной оплатыадминистратор "https://demo.paykeeper.ru/payments/payments"
     * Protected
     * @var string
     */
    public $success_url;
    /**
     * URL для возврата в случае ошибки при оплате "https://demo.paykeeper.ru/payments/payments"
     * Protected
     * @var string
     */
    public $fail_url;
    /**
     * Protected
     * @var string
     */
    public $max_xls_rows;

    /**
     * Setting constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        // set values
        foreach ($parameters as $key => $parameter) {
            if (property_exists($this, $key)) {
                $this->{$key} = $parameter;
            }
        }
    }
}