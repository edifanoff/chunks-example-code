<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\IntegrationPayKeeper;
use App\Models\Invoice;
use App\Models\Journal;
use App\Models\Schedule;
use App\Models\School;
use App\Models\Admin;
use App\Models\Status;
use App\Models\StatusReason;
use App\Models\Template;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\TimeZonesServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class FranchiseLeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('role:boss|admin|teacher');
    }

    /**
     * Show all students.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        return view('pages.admin.franchise-leads', [
            'statuses' => Status::get(),
            'schools' => School::mySchool()->under()->get(),
            'timezoneList' => TimeZonesServices::timeZoneList(),
        ]);
    }

    /**
     * Show all students.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function archive(Request $request)
    {
        return view('pages.admin.franchise-leads-archive', [
            //
        ]);
    }

    /**
     * Show the student .
     *
     * @param Request $request
     * @param $userId
     * @return Factory|View
     */
    public function show(Request $request, $userId)
    {
        $staff = Admin::my()
            ->select(
                'admins.id',
                'admins.name',
                DB::raw('(
                SELECT roles.name
                FROM roles
                WHERE roles.id = admins.level
                ) AS roleName')
            )
            ->get();
        $user = User::under()->with(['school'])->findOrFail($userId);
        $user->password_view = base64_decode($user->password_view);

        if ($user->school->franchise_id !== auth()->user()->franchise_id) {
            abort(404);
        }

        /**
         *
         * 'teacher.name AS teacher_name',
         * 'teacher_replacement.name AS teacher_replacement_name',
         */

        /**
         *             JOIN admins AS teacher ON teacher.id = groups.teacher_id
         * LEFT JOIN admins AS teacher_replacement ON teacher_replacement.id = groups.teacher_replacement_id
         */

        $groups = $user->groups()->with(['school', 'course'])
            ->select(
                'groups.*',

                'teacher.name AS teacher_name',
                'teacher_replacement.name AS teacher_replacement_name',

                DB::raw("(
                SELECT IF( COUNT(journals.id) > 0, 1, 0)
                FROM journals
                JOIN schedules ON schedules.id = journals.schedule_id
             
                WHERE
                    schedules.group_id = groups.id
                    AND journals.user_id = {$user->id}
                ) present")
            )
            ->join('admins AS teacher', 'teacher.id', 'groups.teacher_id')
            ->leftJoin('admins AS teacher_replacement', 'teacher_replacement.id', 'groups.teacher_replacement_id')
            ->get();


        $journals = Journal::join('schedules', 'schedules.id', 'journals.schedule_id')
            ->join('groups', 'groups.id', 'schedules.group_id')
            ->join('schools', 'schools.id', 'groups.school_id')
            ->join('lessons', 'lessons.id', 'schedules.lesson_id')
            ->where('user_id', $user->id)
            ->whereNotNull('schedules.date_end_timestamp')
            ->select(
                DB::raw("DATE_FORMAT(FROM_UNIXTIME(schedules.date_end_timestamp), '%d.%m.%Y') as end_at_timestamp")
            )
            ->orderBy('schedules.date_start_timestamp', 'DESC')->limit(1)->get()->toArray();


        $schoolsByStudent = Journal::join('schedules', 'schedules.id', 'journals.schedule_id')
            ->join('groups', 'groups.id', 'schedules.group_id')
            ->join('schools', 'schools.id', 'groups.school_id')
            ->select('schools.id', 'schools.title')
            ->groupBy('schools.id')
            ->where('journals.user_id', $userId)
            ->get();


        $lessonPay = Invoice::select(DB::raw('SUM(invoices.lesson_number) as total_lesson'))
            ->where('invoices.user_id', $userId)
            ->whereNotNull('invoices.pay_at')
            ->get()->toArray();


        $journalsCount = Journal::join('schedules', 'schedules.id', 'journals.schedule_id')
            ->join('groups', 'groups.id', 'schedules.group_id')
            ->join('schools', 'schools.id', 'groups.school_id')
            ->join('lessons', 'lessons.id', 'schedules.lesson_id')
            ->where('user_id', $user->id)
            ->where('lessons.is_open', 0)
            ->select(
                'schedules.id'
            )
            ->orderBy('schedules.date_start_timestamp', 'DESC')->count();


        return view('pages.admin.profile', [
            'user' => $user,
            'groups' => $groups,
            'staff' => $staff,
            'schools' => School::mySchool()->under()->get(),
            'timeZones' => TimeZonesServices::timeZoneList(),
            'sexLists' => User::sexList(),
            'schoolsByStudent' => $schoolsByStudent,
            'statusList' => Status::notService()->with('reason')->get()->toArray(),
            'reasonList' => StatusReason::active()->get(),
            'payKeeper' => IntegrationPayKeeper::myAccount()->first() ? 1 : 0,
            'templatesEmail' => Template::email()->get(),
            'templatesSMS' => Template::sms()->get(),
            'lessonCount' => $lessonPay[0]['total_lesson'] ?? 0,
            'lessonLeftCount' => $lessonPay[0]['total_lesson'] - $journalsCount,
            'latestDateAbonnement' => $journals[0]['end_at_timestamp'] ?? 'None',
        ]);
    }
}
