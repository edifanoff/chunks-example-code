<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Age;
use App\Models\Courses;
use App\Models\LessonClosed AS Lesson;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('role:methodist|root');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.admin.courses', [
            'statuses' => Courses::statusList(),
            'age' => Age::get(),
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @param $course
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($course)
    {
        $course = Courses::with('lessons')->findOrFail($course);
        $lessons = Lesson::active()->whereNotIn('id', $course->lessons->pluck('id')->toArray())->get();

        return view('pages.admin.course', [
            'course' => $course,
            'lessons' => $lessons,
        ]);
    }

}
