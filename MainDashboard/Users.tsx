import React from 'react';
import Header from '../Header/Header';
import Links from '../Tabs/Tabs';
import {Link} from 'react-router-dom';
import User from '../User/User';
import Icon from '../Icon';

const Users: React.FC = () => {
  return (
    <div className="MainDashboard">
      <Header/>
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <Links>
              <Link to="/">Dashboard</Link>
              {/*<Link to="/settings">Page settings</Link>*/}
              <Link to="/users">Users</Link>
            </Links>
          </div>

          <div className="col-6 text-right">
            <div className="current-date">
              <Icon name="icon-calendar-icon" color="blue"/> <span
              className="title">Tuesday, February 19, 2019</span>

              <Link className='btn-add-new-user' to="/users/create">Add New User</Link>
            </div>


          </div>

        </div>

        <div className='row'>
          <User/>
        </div>

      </div>
    </div>
  );
};


export default Users;