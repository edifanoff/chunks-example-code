export interface Sort<S> {
  field: S | null,
  direction: 'asc' | 'desc',
}

type Comparator<T> = (a: T, b: T) => number

interface BothDirectionComparator<T> {
  asc: Comparator<T>
  desc: Comparator<T>
}

type ComparatorsMap<T, S extends keyof T> = {
  [K in S]: BothDirectionComparator<T>
}

type Aggregator<T, U> = (previous: U, current: T) => U;

class DataProvider<T, S extends keyof T, A extends keyof T> {
  _rows: readonly T[];
  _comparators: ComparatorsMap<T, S>;
  sortState: Sort<S>;
  _total: Pick<T, A> | null;
  _aggregator?: Aggregator<T, Pick<T, A>>;
  _initial?: Pick<T, A>;

  constructor(rows: readonly T[], comparators: ComparatorsMap<T, S>, aggregator?: Aggregator<T, Pick<T, A>>, initial?: Pick<T, A>) {
    this._rows = rows;
    this.sortState = {
      field: null,
      direction: 'asc',
    };
    this._comparators = comparators;
    this._total = null;
    this._aggregator = aggregator;
    this._initial = initial;
  }

  sort(column: S) {
    if (this.sortState.field !== column) {
      this.sortState = {
        field: column,
        direction: 'asc',
      };
      return;
    }
    if (this.sortState.direction === 'asc') {
      this.sortState.direction = 'desc';
      return;
    }
    this.sortState.field = null;
  };

  setRows(rows: T[]) {
    this._rows = rows;
    this._total = null;
  }

  getRows(): readonly T[] {
    if (this.sortState.field === null) {
      return this._rows;
    }

    return [...this._rows].sort(this._comparators[this.sortState.field][this.sortState.direction]);
  }

  aggregate(): Pick<T, A> {
    if (!this._aggregator || !this._initial) {
      throw Error('Wrong configuration.');
    }
    if (this._total === null) {
      this._total = this._rows.reduce(this._aggregator, {...this._initial});
    }
    return this._total;
  }


}

export default DataProvider;